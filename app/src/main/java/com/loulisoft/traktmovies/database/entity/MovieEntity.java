package com.loulisoft.traktmovies.database.entity;

import com.loulisoft.traktmovies.database.TraktDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by iosu on 19/11/16.
 */

@Table(database = TraktDatabase.class)
public class MovieEntity extends BaseModel {

    @PrimaryKey(autoincrement = true)
    @Column
    private int id;

    @Column
    @ForeignKey(saveForeignKeyModel = true)
    private MovieMovieEntity movie;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MovieMovieEntity getMovie() {
        return movie;
    }

    public void setMovie(MovieMovieEntity movie) {
        this.movie = movie;
    }
}
