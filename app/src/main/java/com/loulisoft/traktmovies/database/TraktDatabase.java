package com.loulisoft.traktmovies.database;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by iosu on 21/11/16.
 */

@Database(name = TraktDatabase.NAME, version = TraktDatabase.VERSION)
public class TraktDatabase {

    public static final String NAME = "TraktDatabase";

    public static final int VERSION = 1;
}
