package com.loulisoft.traktmovies.presenter.contract;

import com.loulisoft.traktmovies.database.entity.MovieMovieEntity;
import com.loulisoft.traktmovies.presenter.BasePresenter;
import com.loulisoft.traktmovies.presenter.BaseView;

/**
 * Created by iosu on 23/11/16.
 */

public interface MovieDetailContract {

    interface View extends BaseView<MovieDetailContract.Presenter> {

        void populateViews(MovieMovieEntity movieMovieEntity);

    }

    interface Presenter extends BasePresenter {

        void getMovieEntity(int id);

    }
}
