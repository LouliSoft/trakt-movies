package com.loulisoft.traktmovies.presenter;

import com.loulisoft.traktmovies.database.entity.MovieEntity;
import com.loulisoft.traktmovies.database.entity.MovieEntity_Table;
import com.loulisoft.traktmovies.database.entity.MovieMovieEntity;
import com.loulisoft.traktmovies.presenter.contract.MovieDetailContract;
import com.raizlabs.android.dbflow.sql.language.SQLite;

/**
 * Created by iosu on 23/11/16.
 */

public class MovieDetailPresenter implements MovieDetailContract.Presenter {

    private MovieDetailContract.View mainView;

    public MovieDetailPresenter(MovieDetailContract.View view) {
        mainView = view;
        mainView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void getMovieEntity(int id) {
        if (mainView.isActive()) {
            MovieEntity movieEntity = SQLite.select()
                    .from(MovieEntity.class)
                    .where(MovieEntity_Table.id.is(id))
                    .querySingle();
            MovieMovieEntity movieMovieEntity = movieEntity.getMovie();
            mainView.populateViews(movieMovieEntity);
        }
    }
}
