package com.loulisoft.traktmovies.presenter;

/**
 * Created by iosu on 19/11/16.
 */

public interface BasePresenter {
    void start();
}
