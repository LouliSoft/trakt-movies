package com.loulisoft.traktmovies.presenter.contract;

import com.loulisoft.traktmovies.database.entity.MovieEntity;
import com.loulisoft.traktmovies.presenter.BasePresenter;
import com.loulisoft.traktmovies.presenter.BaseView;

import java.util.List;

/**
 * Created by iosu on 19/11/16.
 */

public interface MainContract {

    interface View extends BaseView<Presenter> {

        void populateViews();

        void setMoviesRecyclerView(List<MovieEntity> list);

        void terminateRefreshing();

        void showConnectionError();

        void showServerError();

        void setLoadingDialog(boolean hasToBeShown);

    }

    interface Presenter extends BasePresenter {

        void downloadMovies(boolean isRefresh, int page);

    }
}
