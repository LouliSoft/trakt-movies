package com.loulisoft.traktmovies.presenter;

/**
 * Created by iosu on 19/11/16.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);

    // to be used in the presenter in order to check if fragment is added
    boolean isActive();
}
