package com.loulisoft.traktmovies.presenter;

import android.content.Context;

import com.loulisoft.traktmovies.database.entity.MovieEntity;
import com.loulisoft.traktmovies.database.entity.MovieMovieEntity;
import com.loulisoft.traktmovies.presenter.contract.MainContract;
import com.loulisoft.traktmovies.utils.ApplicationUtils;
import com.loulisoft.traktmovies.utils.ServiceUtils;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by iosu on 19/11/16.
 */

public class MainPresenter implements MainContract.Presenter {

    private String TAG = MainPresenter.class.getSimpleName();

    private Context mContext;

    private MainContract.View mainView;

    public MainPresenter(Context context, MainContract.View view) {
        mContext = context;
        mainView = view;
        mainView.setPresenter(this);
    }

    @Override
    public void start() {
        if (mainView.isActive()) {
            mainView.populateViews();
            downloadMovies(false, 1);
        }
    }

    @Override
    public void downloadMovies(boolean isRefresh, int page) {
        if (mainView.isActive()) {
            if (!ApplicationUtils.isNetworkAvailable(mContext)) {
                List<MovieEntity> offersList = new SQLite().select().from(MovieEntity.class).queryList();
                if (!offersList.isEmpty()) {
                    mainView.setMoviesRecyclerView(offersList);
                }
                mainView.showConnectionError();
            } else {
                if (!isRefresh) {
                    mainView.setLoadingDialog(true);
                }
                populateList(isRefresh, page);
            }
        }
    }

    private void populateList(final boolean isRefresh, final int page) {

        Request request = new Request.Builder()
                .url(String.format(ServiceUtils.DOWNLOAD_URL, page))
                .addHeader("Content-type", "application/json")
                .addHeader("trakt-api-key", ServiceUtils.TRAKT_API_KEY)
                .addHeader("trakt-api-version", ServiceUtils.TRAKT_API_VERSION)
                .build();

        OkHttpClient client = new OkHttpClient();
        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (mainView.isActive()) {
                    if (response != null && response.isSuccessful()) {

                        if (isRefresh || page == 1) {
                            Delete.tables(MovieEntity.class, MovieMovieEntity.class);
                        }

                        try {
                            JSONArray jsonArray = new JSONArray(response.body().string());
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject data = jsonArray.getJSONObject(i);

                                JSONObject movie = data.getJSONObject("movie");
                                MovieMovieEntity movieMovieEntity = new MovieMovieEntity();
                                movieMovieEntity.setTitle(movie.getString("title"));
                                movieMovieEntity.setYear(movie.getInt("year"));
                                movieMovieEntity.setOverview(movie.getString("overview"));
                                movieMovieEntity.save();

                                MovieEntity movieEntity = new MovieEntity();
                                movieEntity.setMovie(movieMovieEntity);
                                movieEntity.save();
                            }

                            List<MovieEntity> movieEntityList = SQLite.select().from(MovieEntity.class).queryList();
                            mainView.setMoviesRecyclerView(movieEntityList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        mainView.showServerError();
                    }
                    if (isRefresh) {
                        mainView.terminateRefreshing();
                    } else {
                        mainView.setLoadingDialog(false);
                    }
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                if (mainView.isActive()) {
                    if (isRefresh) {
                        mainView.terminateRefreshing();
                    } else {
                        mainView.setLoadingDialog(false);
                    }
                    mainView.showServerError();
                }
            }
        });
    }
}
