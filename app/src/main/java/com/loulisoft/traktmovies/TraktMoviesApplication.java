package com.loulisoft.traktmovies;

import android.app.Application;
import android.content.Context;

import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

/**
 * Created by iosu on 19/11/16.
 */

public class TraktMoviesApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        // This instantiates DBFlow
        FlowManager.init(new FlowConfig.Builder(this).build());
        // add for verbose logging
//        FlowLog.setMinimumLoggingLevel(FlowLog.Level.V);
    }

    public static Context getContext() {
        return context;
    }
}
