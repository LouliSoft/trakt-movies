package com.loulisoft.traktmovies.ui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.loulisoft.traktmovies.R;

/**
 * Created by ilizarraga on 31/05/16.
 */
public class ServerErrorDialog extends BaseDialogFragment implements View.OnClickListener {

    protected Button mOkButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.dialog_fragment_server_error, container);
        TextView titleText = (TextView) view.findViewById(R.id.dialog_fragment_server_error_title);
        titleText.setText(getResources().getText(R.string.alert_error_title));
        TextView messageText = (TextView) view.findViewById(R.id.dialog_fragment_server_error_message);
        messageText.setText(getResources().getText(R.string.alert_error_description));

        mOkButton = (Button) view.findViewById(R.id.dialog_fragment_server_error_ok_button);
        mOkButton.setText(getResources().getText(R.string.alert_error_button));
        mOkButton.setOnClickListener(this);

        setCancelable(false);

        return view;
    }


    @Override
    public void onClick(View v) {

        if (v == mOkButton) {
            try {
                dismiss();
            } catch (IllegalStateException e) {
                dismissAllowingStateLoss();
            }
        }
    }

}
