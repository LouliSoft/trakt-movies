package com.loulisoft.traktmovies.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loulisoft.traktmovies.R;
import com.loulisoft.traktmovies.database.entity.MovieEntity;
import com.loulisoft.traktmovies.database.entity.MovieMovieEntity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by iosu on 19/11/16.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> implements Filterable {

    public static final String TAG = MoviesAdapter.class.getName();

    private Context context;
    private MoviesListener listener;
    private List<MovieEntity> movieEntityList;
    private List<MovieEntity> filteredMovieEntityList;
    private MovieFilter movieFilter;

    public MoviesAdapter(Context context, MoviesListener listener) {
        this.listener = listener;
        this.context = context;
        this.movieEntityList = new ArrayList<>();
        this.filteredMovieEntityList = new ArrayList<>();
    }

    @Override
    public MoviesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_movie, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MoviesAdapter.MyViewHolder holder, int position) {
        final MovieEntity movieEntity = movieEntityList.get(position);
        MovieMovieEntity movieMovieEntity = movieEntity.getMovie();

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onCellClick(movieEntity.getId());
            }
        });

        // FIXME: 23/11/16 I could not get images from service
//        String url = movieMovieEntity.getImageUrl();
//        Picasso.with(context).load(url).placeholder(R.drawable.placeholder).into(holder.image);
        Picasso.with(context).load(R.drawable.placeholder).into(holder.image);

        holder.title.setText(movieMovieEntity.getTitle());
        holder.year.setText(String.valueOf(movieMovieEntity.getYear()));
        holder.overview.setText(movieMovieEntity.getOverview());
    }

    @Override
    public int getItemCount() {
        return (movieEntityList != null) ? movieEntityList.size() : 0;
    }

    /**
     * Sets movies.
     *
     * @param movieEntityList List of MovieEntity.
     */
    public void setMovieList(List<MovieEntity> movieEntityList) {
        this.movieEntityList = movieEntityList;
    }

    @Override
    public Filter getFilter() {
        if (movieFilter == null) {
            movieFilter = new MovieFilter(this, movieEntityList);
        }
        return movieFilter;
    }

    private static class MovieFilter extends Filter {

        private final MoviesAdapter adapter;
        private final List<MovieEntity> originalList;
        private final List<MovieEntity> filteredList;

        private MovieFilter(MoviesAdapter adapter, List<MovieEntity> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(originalList);
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                filteredList.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (final MovieEntity movie : originalList) {
                    if (movie.getMovie().getTitle().contains(filterPattern)) {
                        filteredList.add(movie);
                    }
                    if (movie.getMovie().getOverview().contains(filterPattern)) {
                        filteredList.add(movie);
                    }
                    if (String.valueOf(movie.getMovie().getYear()).contains(filterPattern)) {
                        filteredList.add(movie);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filteredMovieEntityList.clear();
            adapter.filteredMovieEntityList.addAll((ArrayList<MovieEntity>) results.values);
            adapter.movieEntityList = adapter.filteredMovieEntityList;
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * MyViewHolder
     */
    class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout container;
        ImageView image;
        TextView title, year, overview;

        MyViewHolder(View itemView) {
            super(itemView);
            container = (LinearLayout) itemView.findViewById(R.id.cell_movie_container);
            image = (ImageView) itemView.findViewById(R.id.cell_movie_image);
            title = (TextView) itemView.findViewById(R.id.cell_movie_title);
            year = (TextView) itemView.findViewById(R.id.cell_movie_year);
            overview = (TextView) itemView.findViewById(R.id.cell_movie_overview);
        }
    }

    public interface MoviesListener {
        void onCellClick(int id);
    }
}
