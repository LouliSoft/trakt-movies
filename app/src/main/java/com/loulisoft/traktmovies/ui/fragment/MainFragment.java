package com.loulisoft.traktmovies.ui.fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.TextView;

import com.loulisoft.traktmovies.R;
import com.loulisoft.traktmovies.database.entity.MovieEntity;
import com.loulisoft.traktmovies.presenter.contract.MainContract;
import com.loulisoft.traktmovies.ui.activity.MovieDetailActivity;
import com.loulisoft.traktmovies.ui.adapter.MoviesAdapter;
import com.loulisoft.traktmovies.ui.dialog.CustomProgressDialog;
import com.loulisoft.traktmovies.ui.dialog.NoConnectionDialog;
import com.loulisoft.traktmovies.ui.dialog.ServerErrorDialog;

import java.util.List;

/**
 * Created by iosu on 19/11/16.
 */

public class MainFragment extends BaseFragment implements MainContract.View, SearchView.OnQueryTextListener, SearchView.OnCloseListener, SearchView.OnClickListener, MoviesAdapter.MoviesListener {

    public static final String TAG = MainFragment.class.getName();

    private SwipeRefreshLayout mSwipeContainer;
    private RecyclerView mRecyclerView;
    private TextView mNoOffersInfo;
    private MoviesAdapter mAdapter;
    private CustomProgressDialog mCustomProgressDialog;
    private int mPage = 1;
    private boolean mLoading = true;
    private int mPastVisibleItems, mVisibleItemCount, mTotalItemCount;
    private List<MovieEntity> mList;

    private MainContract.Presenter mPresenter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_main;
    }

    @Override
    protected void configView() {
        mSwipeContainer = $(R.id.fragment_main_swipeContainer);
        mRecyclerView = $(R.id.fragment_main_recyclerview);
        mNoOffersInfo = $(R.id.fragment_main_textView);
    }

    @Override
    protected void startPresenter() {
        if (mPresenter != null) {
            mPresenter.start();
        }
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void populateViews() {
        mNoOffersInfo.setText(getContext().getString(R.string.text_emptycard));

        final LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        mAdapter = new MoviesAdapter(getContext(), this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) { //check for scroll down
                    mVisibleItemCount = manager.getChildCount();
                    mTotalItemCount = manager.getItemCount();
                    mPastVisibleItems = manager.findFirstVisibleItemPosition();

                    if (mLoading) {
                        if ((mVisibleItemCount + mPastVisibleItems) >= mTotalItemCount) {
                            mLoading = false;
                            mPresenter.downloadMovies(false, ++mPage);
                        }
                    }
                }
            }
        });

        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mPresenter.downloadMovies(true, 1);
            }

        });
        mSwipeContainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override
    public void setMoviesRecyclerView(final List<MovieEntity> list) {
        mList = list;
        if (list != null && !list.isEmpty()) {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    mAdapter.setMovieList(list);
                    mAdapter.notifyDataSetChanged();
                    mLoading = true;

                    mNoOffersInfo.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                }
            });
        } else {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    mRecyclerView.setVisibility(View.GONE);
                    mNoOffersInfo.setVisibility(View.VISIBLE);
                }
            });
        }

    }

    @Override
    public void terminateRefreshing() {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                mSwipeContainer.setRefreshing(false);
                mSwipeContainer.destroyDrawingCache();
                mSwipeContainer.clearAnimation();
            }
        });
    }

    @Override
    public void showConnectionError() {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                final NoConnectionDialog noConnectionDialog = new NoConnectionDialog();
                noConnectionDialog.setOnTryAgainListener(new NoConnectionDialog.OnTryAgainListener() {
                    @Override
                    public void onTryAgainListener() {
                        noConnectionDialog.dismiss();
                        mPresenter.downloadMovies(false, mPage);
                    }
                });
                noConnectionDialog.show(getActivity().getSupportFragmentManager(), "fragment_no_connection");
            }
        });
    }

    @Override
    public void showServerError() {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                ServerErrorDialog serverErrorDialog = new ServerErrorDialog();
                serverErrorDialog.show(getActivity().getSupportFragmentManager(), "fragment_server_error");
            }
        });
    }

    @Override
    public void setLoadingDialog(boolean hasToBeShown) {
        if (hasToBeShown) {
            mCustomProgressDialog = new CustomProgressDialog();
            mCustomProgressDialog.show(getActivity().getSupportFragmentManager(), "fragment_custom_progress");
        } else if (mCustomProgressDialog != null) {
            try {
                mCustomProgressDialog.dismiss();
            } catch (IllegalStateException e) {
                mCustomProgressDialog.dismissAllowingStateLoss();
            }
        }
    }

    @Override
    public boolean onQueryTextChange(String query) {
        mAdapter.getFilter().filter(query);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mAdapter.getFilter().filter(query);
        return true;
    }

    @Override
    public boolean onClose() {
        mSwipeContainer.setEnabled(true);
        return false;
    }

    @Override
    public void onClick(View view) {
        mSwipeContainer.setEnabled(false);
    }

    @Override
    public void onCellClick(int id) {
        getContext().startActivity(MovieDetailActivity.makeIntent(getContext(), id));
    }
}
