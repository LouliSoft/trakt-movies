package com.loulisoft.traktmovies.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.loulisoft.traktmovies.R;
import com.loulisoft.traktmovies.presenter.MainPresenter;
import com.loulisoft.traktmovies.ui.fragment.MainFragment;
import com.loulisoft.traktmovies.utils.ActivityUtils;

/**
 * Created by iosu on 19/11/16.
 */

public class MainActivity extends BaseActivity {

    private MainFragment fragment;

    public static Intent makeIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        setUpToolbar();

        fragment = (MainFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (fragment == null) {
            fragment = MainFragment.newInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.contentFrame);
        }

        new MainPresenter(this, fragment);
    }

    /**
     * Sets up toolbar.
     */
    private void setUpToolbar() {

        View toolbarLayout = $(R.id.app_bar);
        if (toolbarLayout != null) {

            setToolbarLayout(toolbarLayout);
            setToolbarTitle(getResources().getString(R.string.app_name));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        SearchView.SearchAutoComplete theTextArea = (SearchView.SearchAutoComplete)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        theTextArea.setHint(R.string.action_search);//or any color that you want
        theTextArea.setTextColor(Color.WHITE);//or any color that you want
        theTextArea.setHintTextColor(Color.LTGRAY);//or any color that you want
        searchView.setOnQueryTextListener(fragment);
        searchView.setOnCloseListener(fragment);
        searchView.setOnSearchClickListener(fragment);

        return true;
    }
}
