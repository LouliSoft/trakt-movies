package com.loulisoft.traktmovies.ui.fragment;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by iosu on 19/11/16.
 */

public abstract class BaseFragment extends Fragment {

    private View parentView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(getLayoutResource(), container, false);
        configView();
        return parentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        startPresenter();
    }

    protected <T extends View> T $(@IdRes int resId) {
        if (parentView != null)
            return (T) parentView.findViewById(resId);
        return null;

    }

    protected View getMainView() {
        return parentView;
    }

    protected abstract int getLayoutResource();

    protected abstract void configView();

    protected abstract void startPresenter();

}
