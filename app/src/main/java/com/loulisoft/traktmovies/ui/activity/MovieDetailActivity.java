package com.loulisoft.traktmovies.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.loulisoft.traktmovies.R;
import com.loulisoft.traktmovies.presenter.MovieDetailPresenter;
import com.loulisoft.traktmovies.ui.fragment.MovieDetailFragment;
import com.loulisoft.traktmovies.utils.ActivityUtils;

/**
 * Created by iosu on 23/11/16.
 */

public class MovieDetailActivity extends BaseActivity {

    private static final String EXTRA_ID = "extra_id";

    public static Intent makeIntent(Context context, int id) {
        return new Intent(context, MovieDetailActivity.class).putExtra(EXTRA_ID, id);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        setUpToolbar();

        MovieDetailFragment fragment = (MovieDetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (fragment == null) {
            fragment = MovieDetailFragment.newInstance(getIntent().getIntExtra(EXTRA_ID, 0));

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.contentFrame);
        }

        new MovieDetailPresenter(fragment);
    }

    /**
     * Sets up toolbar.
     */
    private void setUpToolbar() {

        View toolbarLayout = $(R.id.app_bar);
        if (toolbarLayout != null) {

            setToolbarLayout(toolbarLayout);
            setToolbarTitle(getResources().getString(R.string.app_name));
            showToolbarBackButton();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
