package com.loulisoft.traktmovies.ui.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.loulisoft.traktmovies.R;

/**
 * Created by ilizarraga on 31/05/16.
 */
public class NoConnectionDialog extends BaseDialogFragment implements View.OnClickListener {

    private Button mConnectButton, mDismissButton;
    private OnTryAgainListener onTryAgainListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.dialog_fragment_no_connection, container);
        TextView titleText = (TextView) view.findViewById(R.id.dialog_fragment_no_connection_title);
        titleText.setText(getResources().getText(R.string.alert_offline_title));
        TextView messageText = (TextView) view.findViewById(R.id.dialog_fragment_no_connection_message);
        messageText.setText(getResources().getText(R.string.alert_offline_message));
        mConnectButton = (Button) view.findViewById(R.id.dialog_fragment_no_connection_connect_button);
        mConnectButton.setText(getResources().getText(R.string.alert_offline_button_connect));
        mConnectButton.setOnClickListener(this);
        mDismissButton = (Button) view.findViewById(R.id.dialog_fragment_no_connection_dismiss_button);
        mDismissButton.setText(getResources().getText(R.string.alert_offline_button_dismiss));
        mDismissButton.setOnClickListener(this);

        setCancelable(false);
        return view;
    }

    public void setOnTryAgainListener(OnTryAgainListener listener) {
        onTryAgainListener = listener;
    }

    @Override
    public void onClick(View v) {
        if (v == mDismissButton) {
            try {
                dismiss();
            } catch (IllegalStateException e) {
                dismissAllowingStateLoss();
            }
        } else if (v == mConnectButton) {
            if (onTryAgainListener != null) {
                onTryAgainListener.onTryAgainListener();
            }
        }
    }

    public interface OnTryAgainListener {

        void onTryAgainListener();
    }

}
