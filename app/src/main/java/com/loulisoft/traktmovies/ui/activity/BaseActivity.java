package com.loulisoft.traktmovies.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.loulisoft.traktmovies.R;

/**
 * Created by iosu on 19/11/16.
 */

public class BaseActivity extends AppCompatActivity {

    protected Toolbar mToolbar;

    protected <T extends View> T $(int viewId) {
        return (T) findViewById(viewId);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Toolbar layout.
     *
     * @param toolbarLayout Toolbar layout.
     */
    public void setToolbarLayout(View toolbarLayout) {
        if ((toolbarLayout != null) && (toolbarLayout.findViewById(R.id.toolbar) != null)) {
            mToolbar = (Toolbar) toolbarLayout.findViewById(R.id.toolbar);
            setSupportActionBar(mToolbar);
        }
    }

    /**
     * Shows toolbar back button.
     */
    protected void showToolbarBackButton() {

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_keyboard_backspace_white_24dp);
        }
    }

    /**
     * Sets toolbar title.
     *
     * @param title Title
     */
    public void setToolbarTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("");
        }
        if (mToolbar != null) {
            mToolbar.setTitle(title);
        }
    }

    /**
     * Sets toolbar home action.
     *
     * @param item MenuItem
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
