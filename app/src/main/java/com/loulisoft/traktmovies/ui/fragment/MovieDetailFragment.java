package com.loulisoft.traktmovies.ui.fragment;

import android.widget.ImageView;
import android.widget.TextView;

import com.loulisoft.traktmovies.R;
import com.loulisoft.traktmovies.database.entity.MovieMovieEntity;
import com.loulisoft.traktmovies.presenter.contract.MovieDetailContract;
import com.squareup.picasso.Picasso;

/**
 * Created by iosu on 23/11/16.
 */

public class MovieDetailFragment extends BaseFragment implements MovieDetailContract.View {

    private ImageView image;
    private TextView title, year, overview;
    private int id;

    private MovieDetailContract.Presenter mPresenter;

    public static MovieDetailFragment newInstance(int id) {
        MovieDetailFragment movieDetailFragment = new MovieDetailFragment();
        movieDetailFragment.id = id;
        return movieDetailFragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_movie_detail;
    }

    @Override
    protected void configView() {
        image = $(R.id.fragment_movie_detail_image);
        title = $(R.id.fragment_movie_detail_title);
        year = $(R.id.fragment_movie_detail_year);
        overview = $(R.id.fragment_movie_detail_overview);

        mPresenter.getMovieEntity(id);
    }

    @Override
    public void setPresenter(MovieDetailContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    protected void startPresenter() {
        if (mPresenter != null) {
            mPresenter.start();
        }
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void populateViews(MovieMovieEntity movieMovieEntity) {

        // FIXME: 23/11/16 I could not get images from service
//        String url = movieMovieEntity.getImageUrl();
//        Picasso.with(context).load(url).placeholder(R.drawable.placeholder).into(holder.image);
        Picasso.with(getContext()).load(R.drawable.placeholder).into(image);

        title.setText(movieMovieEntity.getTitle());
        year.setText(String.valueOf(movieMovieEntity.getYear()));
        overview.setText(movieMovieEntity.getOverview());
    }
}
