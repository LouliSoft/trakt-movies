package com.loulisoft.traktmovies.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.loulisoft.traktmovies.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by iosu on 22/11/16.
 */

public class SplashActivity extends AppCompatActivity {

    // Set the duration of the splash screen
    public static final long SPLASH_SCREEN_DELAY = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        timer();

    }

    private void timer() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                startActivity(MainActivity.makeIntent(SplashActivity.this));
                finish();
            }
        };

        // Simulate a loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);
    }

}
